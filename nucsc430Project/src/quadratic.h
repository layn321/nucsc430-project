/**----------------------------------------------------------------------------
 * CSC 430 July 2012 Final Project
 *
 * @author Layne Frederiksen
 * NU ID: 021673504
 *
 * @file quadratic.h
 *
 * Description:
 * @brief Solve quadratic equations via quadratic formula if the disriminant
 * is non-negative. Provides the functionality required by the Project
 * description as well as output the complex number calculated roots regardless.
 *
 * @date Created on: July 15, 2012
 * @date Modified on: August 5, 2012
 *-----------------------------------------------------------------------------
 */

#ifndef QUADRATIC_H_
#define QUADRATIC_H_

#include <complex>
#include <iostream>

namespace NU_021673504_CSC430_201207_Project {


class quadratic {
public:
	quadratic(double _a = 1, double _b = 0, double _c = 0);
	virtual ~quadratic();
	double getA() const;
	void setA(double a);
	double getB() const;
	void setB(double b);
	double getC() const;
	void setC(double c);
	double getDiscriminant();
	double getPositiveRoot();
	double getNegativeRoot();
	std::complex<double> getComplexPositiveRoot();
	std::complex<double> getComplexNegativeRoot();
	void print();

private:

	double a, b, c;

};

} /* namespace NU_021673504_CSC430_201207_Project */
#endif /* QUADRATIC_H_ */

