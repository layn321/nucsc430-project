/**----------------------------------------------------------------------------
 * CSC 430 July 2012 Final Project
 *
 * @author Layne Frederiksen
 * NU ID: 021673504
 *
 * @file quadratic.cpp
 *
 * Description:
 * @brief see quadratic.h
 *
 * @date Created on: July 15, 2012
 * @date Modified on: August 5, 2012
 *-----------------------------------------------------------------------------
 */

#include <stdexcept>
#include <string>
#include "quadratic.h"

namespace NU_021673504_CSC430_201207_Project {

// CONSTRUCTOR(S)

quadratic::quadratic(double _a, double _b, double _c) : a(_a), b(_b), c(_c) {

	if(a == 0) {
		std::cout << "a can not be zero because division by zero is undefined. a will be set to 1."
				  << std::endl;
		a = 1;
	}

}

// DESTRUCTOR(S)

quadratic::~quadratic() {
	// TODO Auto-generated destructor stub
}

// MEMBER FUCTIONS/METHODS

double quadratic::getA() const {
	return a;
}

void quadratic::setA(double a) {
	this->a = a;
}

double quadratic::getB() const {
	return b;
}

void quadratic::setB(double b) {
	this->b = b;
}

double quadratic::getC() const {
	return c;
}

void quadratic::setC(double c) {
	this->c = c;
}

double quadratic::getDiscriminant() {

	return (b*b-4*a*c);
}

double quadratic::getPositiveRoot() {

	using namespace std;

	double result;

	// can't calculate if discriminant is negative
	if(getDiscriminant() < 0) {
		throw string("Can't calculate root because discriminant is negative.");
	} else {
		result = ((-b + sqrt(b * b - 4 * a * c))) / (2*a);
	}

	return result;
}

double quadratic::getNegativeRoot() {

	using namespace std;

	double result;

	// can't calculate if discriminant is negative
	if(getDiscriminant() < 0) {
		throw string("Can't calculate root because discriminant is negative.");
	} else {
		result = ((-b - sqrt(b * b - 4 * a * c))) / (2*a);
	}

	return result;
}

std::complex<double> quadratic::getComplexPositiveRoot() {

	return (-b + sqrt(std::complex<double>(b*b-4*a*c)) ) / (2*a);
}

std::complex<double> quadratic::getComplexNegativeRoot() {

	return (-b - sqrt(std::complex<double>(b*b-4*a*c)) ) / (2*a);
}

void quadratic::print() {
	std::cout << "Positive root: " << this->getPositiveRoot() << std::endl;
	std::cout << "Negative root: " << this->getNegativeRoot() << std::endl << std::endl;
}

} /* namespace NU_021673504_CSC430_201207_Project */
