/**----------------------------------------------------------------------------
 * CSC 430 July 2012 Final Project
 *
 * @author Layne Frederiksen
 * NU ID: 021673504
 *
 * @file nucsc430Project.cpp
 *
 * Description:
 * @brief driver program for the quadratic class demonstrating capabilities
 *
 * @date Created on: July 15, 2012
 * @date Modified on: August 5, 2012
 *-----------------------------------------------------------------------------
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include "quadratic.h"

using namespace std;
using namespace NU_021673504_CSC430_201207_Project;

void printDivide(char c);

int main() {
	double _a;
    double _b;
	double _c;



	char choice = 'C';

	while(choice != 'Q') {
		try {

			// very basic menu choices
			switch(choice) {
			case 'C': choice = 'C'; break;
			case 'Q': choice = 'Q'; return EXIT_SUCCESS; // not good programming but works for this case
			default : throw string("You didn't make a proper choice. Please choose from the following:\n"
					 "Choices: (C)ontinue (Q)uit");
			} // end switch


			// temp variables
			char var, carrot;
			double power;
			string tempString;


			cout << "[NOTE: I am not checking for typos]" << endl;
			cout << "Enter a quadratic equation in the form: ax^2+bx+c: ";
			cout << endl << endl;

			getline(cin, tempString);
			stringstream str(tempString);

			// get the value for a, b, c from the equation
			str >> _a >> var >> carrot >> power >> _b >> var >> _c;

			// new quadritic object
			quadratic q1(_a, _b, _c);

			cout << "Discriminant: " << q1.getDiscriminant() << endl << endl;

			// display the +/- roots if the Discriminant is >= 0 otherwise print an error
			try
			{
				cout << "Positive Root: " << q1.getPositiveRoot() << endl;
				cout << "Negative Root: " << q1.getNegativeRoot() << endl << endl;
				cout << "Call to the \"print\" method for proof of implementation." << endl;
				q1.print();
			}
			catch(string& e) {
				cout << e << endl << endl;
			}

			// Output the complex roots --> outside the original requirements
			cout << "[Positive Complex Root: " << q1.getComplexPositiveRoot().real();
			if(q1.getComplexPositiveRoot().imag() != 0) // include the imaginary part?
				cout << ((q1.getComplexPositiveRoot().imag()<0)?"":"+") << q1.getComplexPositiveRoot().imag() << "i";
			cout << "]" << endl;

			cout << "[Negative Complex Root: " << q1.getComplexNegativeRoot().real();
			if(q1.getComplexPositiveRoot().imag() != 0)// include the imaginary part?
				cout << ((q1.getComplexNegativeRoot().imag()<0)?"":"+") << q1.getComplexNegativeRoot().imag() << "i";
			cout << "]"<< endl << endl;


		} catch (string& e) {

			cout << "There was a problem." << endl;
			printDivide('-');
			cout << e << "\n";
			printDivide('-');
			cout << "Let's try again. ";

		}

		// User Menu prompt
		cout << "Do you want to (C)ontinue or (Q)uit (Enter 'C' or 'Q')? > " << endl;
		cin >> choice;
		cin.ignore();

	} // end while
	return EXIT_SUCCESS;
}

void printDivide(char c)
{
	std::cout << std::setw(80) << std::setfill(c) << c;
	std::cout << std::endl;
}
